package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static ru.ilinovsg.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher{
    List<Listener> listeners = new ArrayList<>();

    @Override
    public void addListener(Listener listener) {
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void notify(String command) throws ProjectNotFoundException, TaskNotFoundException {
        for (Listener listener : listeners){
            listener.update(command);
        }
    }

    @Override
    public void start() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                notify(command);
            }
            catch (ProjectNotFoundException | TaskNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
